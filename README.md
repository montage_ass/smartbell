# Smartbell on D1 Mini ESP8266

Sketch to interface door intercom (bell, buzzer) via mqtt.

This is an experimental fork of the original sketch by Anton Bracke, see here: https://beechy.de/smart-doorbell/

## Changes

* updated to PubSubClient v2.7
* completely reworked bell-state; integrated NTPClient library: it will now send a timestamp of the last time the bell has been rung instead of 0 or 1
* added auto-open feature: when activated, ringing the bell will automatically trigger the door buzzer
* changed door-switch and auto-open subscription QoS to 1 instead of 0
* added additional debug messages e.g. print IP & MAC address when connected to WiFi

## Setup

Copy config.example.h to config.h and edit settings

## Required Libraries

* PubSubClient: https://github.com/knolleary/pubsubclient
* NTPClient: https://github.com/taranais/NTPClient/

## Credits

* https://github.com/anbraten/esp-bell
* https://github.com/mertenats/Itead_Sonoff

![MQTT_Dash1](https://i.ibb.co/PFcyXMV/mqtt-dash-1.jpg)
![MQTT_Dash2](https://i.ibb.co/XjpVQ04/mqtt-dash-2.jpg)